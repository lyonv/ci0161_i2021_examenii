import json
import pandas as pd

image_csv = pd.read_csv("tarot.csv")
with open('tarot_meanings_extract.json', 'r') as meanings_file:
    tarot = json.load(meanings_file)["interpretations"]
    for card, (_, row) in zip(tarot, image_csv.iterrows()):
        if card["suit"] == "coins":
            card["suit"] == "pentacles"
        card["name"] = card["name"].replace("Coins", "Pentacles")
        
        card["meaning_upright"] = "\n".join(card["meanings"]["light"])
        card["meaning_reversed"] = "\n".join(card["meanings"]["shadow"])
        card["keywords"] = ", ".join(card["keywords"])
        card.pop("meanings")
        card.pop("fortune_telling")
        card.pop("characterizations", None)
        
        card["image"] = row["image"]
tarot = {"MyTarot" : tarot}
with open('Tarot.json', 'w') as tarot_out:
    json.dump(tarot, tarot_out, indent = 4)