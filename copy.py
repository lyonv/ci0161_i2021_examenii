from shutil import copyfile

file_name = "Tarot"
extension = "json"

times = 45

for i in range(1, times+1):
    old_file = ".".join([file_name,extension])
    new_file = ".".join([file_name + str(i),extension])
    copyfile(old_file, new_file)
